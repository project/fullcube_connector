<?php

/**
 * @file
 * Mapping admin settings functions.
 */

/**
 * Menu callback for the mapping configuration screen.
 */
function fullcube_field_settings_mapping($form, &$form_state) {
  $user_field_options = fullcube_field_mapping_get_user_field_options();
  $fullcube_field_options = fullcube_field_mapping_get_Fullcube_field_options();
  $map = _fullcube_field_mapping_retrieve();

  // Add an empty row for saving a new mapping.
  $map[] = array();

  foreach ($map as $mid => $mapping) {
    $selected_field = isset($form_state['values'][$mid]['field']) ? $form_state['values'][$mid]['field'] :
      (isset($map[$mid]['field']) ? $map[$mid]['field'] : '');
    $form[$mid] = array(
      'fid' => array(
        '#type' => 'select',
        '#title' => t('Fullcube field'),
        '#title_display' => 'invisible',
        '#options' => $fullcube_field_options,
        '#default_value' => isset($map[$mid]['fid']) ? $map[$mid]['fid'] : '',
        '#empty_option' => t('- Select a data field -'),
        '#attributes' => array('class' => array('fullcube-field-select', 'mid-' . $mid)),
      ),
      'separator' => array(
        '#markup' => '=>',
      ),
      'field' => array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#title_display' => 'invisible',
        '#options' => $user_field_options,
        '#empty_option' => t('- Select a field -'),
        '#default_value' => isset($map[$mid]['field']) ? $map[$mid]['field'] : '',
        '#attributes' => array('class' => array('field-select', 'mid-' . $mid)),
        '#ajax' => array(
          'callback' => '_fullcube_field_ajax_callback',
          'wrapper' => 'fullcube-columns-replace-' . $mid,
        ),
      ),
      'column' => array(
        '#type' => 'select',
        '#title' => t('Column'),
        '#title_display' => 'invisible',
        '#prefix' => '<div id="fullcube-columns-replace-' . $mid . '">',
        '#suffix' => '</div>',
        '#options' => _fullcube_field_mapping_get_columns($selected_field),
        '#default_value' => isset($map[$mid]['column']) ? $map[$mid]['column'] : '',
      ),
    );
  }

  if (count($form)) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  }

  $form['#tree'] = TRUE;

  return $form;
}

/**
 * Validation callback for the field mapping form.
 *
 * @see fullcube_field_settings_mapping()
 */
function fullcube_field_settings_mapping_validate($form, &$form_state) {
  $values = $form_state['values'];
  $mapped = array();

  foreach (element_children($values) as $mid) {
    if (is_numeric($mid)) {
      if (!empty($values[$mid]['fid']) && !empty($values[$mid]['field'])) {
        if (!isset($mapped[$values[$mid]['field']])) {
          $mapped[$values[$mid]['field']] = TRUE;
        }
        else {
          // Skip for address field with same field and different column
          if(!in_array($values[$mid]['column'], array('locality', 'administrative_area', 'postal_code', 'country',
        'thoroughfare'))) {
            form_set_error("{$mid}][field", t('Each user field can only have one fullcube field mapped to it'));
          }
        }
      }
    }
  }
}

/**
 * Submit handler to update changed Drupal to Fullcube data mapping.
 *
 * @see fullcube_field_settings_mapping()
 */
function fullcube_field_settings_mapping_submit($form, &$form_state) {
  $values = $form_state['values'];
  $map = array();

  foreach (element_children($values) as $mid) {
    if (is_numeric($mid)) {
      // All field mappings should include a bundle, except for legacy
      // (non-fieldable) entity fields.
      if (!empty($values[$mid]['fid']) && !empty($values[$mid]['field'])) {
        $path_field = db_select('fullcube_field_mapping_field', 'wm')
          ->fields('wm', array('path'))
          ->condition('wm.fid', $values[$mid]['fid'])
          ->range(0, 1)
          ->execute()
          ->fetchField();
        $map[$mid] = array(
          'fid' => $values[$mid]['fid'],
          'fcpath' => $path_field,
          'field' => $values[$mid]['field'],
          'column' => isset($values[$mid]['column']) ? $values[$mid]['column'] : 'value',
        );
      }
    }
  }
  variable_set('fullcube_field_mapping_map', $map);
  drupal_set_message(t('Profile to Fullcube data mapping has been updated.'));
}

/**
 * Theme Fullcube field mapping form.
 *
 * @ingroup themeable
 */
function theme_fullcube_field_settings_mapping($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    // Skip form control elements.
    if (array_key_exists('separator', $form[$key])) {
      $field = &$form[$key];

      // Add the row.
      $row = array();
      $row[] = drupal_render($field['fid']);
      $row[] = drupal_render($field['separator']);
      $row[] = drupal_render($field['field']);
      $row[] = drupal_render($field['column']);
      $rows[] = array('data' => $row);
    }
  }
  $header = array(
    t('Fullcube Data Field'),
    '',
    t('Drupal User Field'),
    t('Field Column'),
  );

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Menu callback: Generate a form to manage Fulcube profile fields.
 *
 * @ingroup forms
 * @see _fullcube_field_settings_fields_validate()
 * @see _fullcube_field_settings_fields_submit()
 */
function fullcube_field_settings_fields($form, &$form_state) {
  $fields = db_query("SELECT fid, title, path FROM {fullcube_field_mapping_field}");
  while ($field = $fields->fetchObject()) {
    $admin_field_path = 'admin/config/fullcube/fields';
    $form[$field->fid] = array(
      'title' => array(
        '#markup' => $field->title,
      ),
      'path' => array(
        '#markup' => $field->path,
      ),
      'edit' => array(
        '#type' => 'link',
        '#title' => t('edit'),
        '#href' => $admin_field_path . '/edit/' . $field->fid,
        '#options' => array('attributes' => array('title' => t('Edit field.'))),
      ),
      'delete' => array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => $admin_field_path . '/delete/' . $field->fid,
        '#options' => array('attributes' => array('title' => t('Delete field.'))),
      ),
    );
  }

  // Additional row: add new field.
  $form['_edit_field'] = array(
    'title' => array(
      '#type' => 'textfield',
      '#title' => t('New field title'),
      '#title_display' => 'invisible',
      '#size' => 15,
      '#attributes' => array('class' => array('fullcube-field-title-input')),
      '#description' => t('Title'),
      '#prefix' => '<div class="add-new-placeholder">' . t('Add new field') . '</div>',
    ),
    'path' => array(
      '#type' => 'textfield',
      '#title' => t('New field path'),
      '#title_display' => 'invisible',
      '#size' => 30,
      '#attributes' => array('class' => array('fullcube-path-input')),
      '#description' => t('Fullcube data path'),
      '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save new field'),
  );
  $form['actions']['reset'] = array(
    '#type' => 'link',
    '#title' => t('Reset to defaults'),
    '#href' => 'admin/config/fullcube/fields/reset',
  );

  $form['#tree'] = TRUE;
  $form['#validate'][] = '_fullcube_field_settings_fields_validate';
  $form['#submit'][] = '_fullcube_field_settings_fields_submit';

  return $form;
}

/**
 * Validate submission.
 */
function _fullcube_field_settings_fields_validate($form, &$form_state) {
  $field = $form_state['values']['_edit_field'];

  // Missing title.
  if (!$field['title']) {
    form_set_error('_edit_field][title', t('You need to provide a title.'));
  }
  // Missing data path.
  if (!$field['path']) {
    form_set_error('_edit_field][path', t('You need to provide a data path.'));
  }

  $query = db_select('fullcube_field_mapping_field');
  $query->fields('fullcube_field_mapping_field', array('fid'));

  if (isset($field['fid'])) {
    $query->condition('fid', $field['fid'], '<>');
  }
  $query_path = clone $query;

  $title = $query
    ->condition('title', $field['title'])
    ->execute()
    ->fetchField();
  if ($title) {
    form_set_error('_edit_field][title', t('The specified title is already in use.'));
  }
  $path = $query_path
    ->condition('path', $field['path'])
    ->execute()
    ->fetchField();
  if ($path) {
    form_set_error('_edit_field][path', t('The specified path is already in use.'));
  }
}

/**
 * Process save / edit submissions of field mapping.
 */
function _fullcube_field_settings_fields_submit($form, &$form_state) {
  $field = $form_state['values']['_edit_field'];

  // Remove all elements that are not fullcube_field_mapping_field columns.
  $values = array_intersect_key($field, array_flip(array('title', 'path')));
  if (!isset($field['fid'])) {
    db_insert('fullcube_field_mapping_field')
      ->fields(array('title' => $values['title'], 'path' => $values['path']))
      ->execute();
    drupal_set_message(t('The field has been created.'));
    watchdog('fullcube_field_mapping', 'Fullcube profile field %field added with path %path.', array('%field' => $field['title'], '%path' => $field['path']), WATCHDOG_NOTICE, l(t('view'), 'admin/config/fullcube/fields'));
  }
  else {
    db_update('fullcube_field_mapping_field')
      ->fields(array('title' => $values['title'], 'path' => $values['path']))
      ->condition('fid', $field['fid'])
      ->execute();
    drupal_set_message(t('The field has been updated.'));
  }
  cache_clear_all();
  menu_rebuild();

  $form_state['redirect'] = 'admin/config/fullcube/fields';
}

/**
 * Returns HTML for the Fullcube fields overview page.
 *
 * @ingroup themeable
 */
function theme_fullcube_field_settings_fields($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    // Skip form control elements.
    if (array_key_exists('path', $form[$key])) {
      $field = &$form[$key];

      // Add the row.
      $row = array();
      $row[] = drupal_render($field['title']);
      $row[] = drupal_render($field['path']);
      $row[] = drupal_render($field['edit']);
      $row[] = drupal_render($field['delete']);
      $rows[] = array('data' => $row);
    }
  }

  $header = array(t('Title'), t('Path'));
  $header[] = array('data' => t('Operations'), 'colspan' => 2);

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Menu callback: Generate a form to edit an Fullcube profile field.
 *
 * @ingroup forms
 * @see _fullcube_field_settings_fields_validate()
 * @see _fullcube_field_settings_fields_submit()
 */
function fullcube_field_settings_fields_edit($form, &$form_state, $arg = NULL) {
  if (is_numeric($arg)) {
    $fid = $arg;
    $field = db_query('SELECT fid, title, path FROM {fullcube_field_mapping_field} WHERE fid = :fid', array('fid' => $fid))->fetchAssoc();

    if (!$field) {
      drupal_not_found();
      drupal_exit();
    }
    drupal_set_title(t('Edit %title Fullcube field', array('%title' => $field['title'])), PASS_THROUGH);
    $form['_edit_field']['fid'] = array(
      '#type' => 'value',
      '#value' => $fid,
    );
  }
  else {
    drupal_not_found();
    drupal_exit();
  }

  $form['_edit_field']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $field['title'],
    '#attributes' => array('class' => array('fullcube-field-title-input')),
    '#description' => t('The title of the field. The title is shown in the
    mapping form next to the data path. An example title is "Email". '),
  );
  $form['_edit_field']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Fullcube data path'),
    '#default_value' => $field['path'],
    '#attributes' => array('class' => array('fullcube-path-input')),
    '#description' => t("The path to the data within the Fullcube schema,
    delimited by dot-notation. An example path is <code>gender</code> or
    <code>primaryAddress.city</code>."),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save field'),
  );

  $form['#tree'] = TRUE;
  $form['#validate'][] = '_fullcube_field_settings_fields_validate';
  $form['#submit'][] = '_fullcube_field_settings_fields_submit';

  return $form;
}

/**
 * Deletes a Fullcube field from the Drupal to Fullcube mapping.
 */
function fullcube_field_settings_fields_delete($form, &$form_state, $fid = NULL) {
  $field = db_query("SELECT title FROM {fullcube_field_mapping_field} WHERE fid = :fid", array(':fid' => $fid))->fetchObject();
  if (!$field) {
    drupal_not_found();
    drupal_exit();
  }
  $form['fid'] = array('#type' => 'value', '#value' => $fid);
  $form['title'] = array('#type' => 'value', '#value' => $field->title);

  return confirm_form($form,
    t('Are you sure you want to delete the Fullcube field %field?', array('%field' => $field->title)), 'admin/config/fullcube/fields',
    t('This action cannot be undone. The Drupal to Fullcube data mappings using this field will be deleted as well.'),
    t('Delete'), t('Cancel'));
}

/**
 * Process an Fullcube field delete form submission.
 */
function fullcube_field_settings_fields_delete_submit($form, &$form_state) {
  db_delete('fullcube_field_mapping_field')
    ->condition('fid', $form_state['values']['fid'])
    ->execute();

  cache_clear_all();

  drupal_set_message(t('The Fullcube field %field has been deleted.', array('%field' => $form_state['values']['title'])));
  watchdog('fullcube_field_mapping', 'Fullcube field %field deleted.', array('%field' => $form_state['values']['title']), WATCHDOG_NOTICE, l(t('view'), 'admin/config/people/fullcube_field/fields'));

  $form_state['redirect'] = 'admin/config/fullcube/fields';
}

/**
 * Menu callback; confirmation form.
 */
function fullcube_field_settings_fields_reset($form, &$form_state) {
  return confirm_form($form, t('Are you sure you want to revert to the default Fullcube Webhook profile fields?'), 'admin/config/fullcube/fields', t('Any new fields that you have created will be lost. Any existing mappings will be edelted as well. This action cannot be undone.'), t('Reset'));
}

/**
 * Process fields reset form submissions.
 *
 * Reset Fullcube fields by deleting all existing fields and creating default
 * ones (those that are created at module installation).
 */
function fullcube_field_settings_fields_reset_submit($form, &$form_state) {
  db_query("DELETE FROM {fullcube_field_mapping_field}");

  _fullcube_field_mapping_insert_defaults();

  drupal_set_message(t('The Fullcube Webhook profile fields have been reverted to the defaults.'));
  $form_state['redirect'] = 'admin/config/fullcube/fields';
}

/********************************* Helper Functions ************************************************/

/**
 * Construct an array of options for the user field dropdown.
 *
 * @see fullcube_field_settings_mapping()
 */
function fullcube_field_mapping_get_user_field_options() {
  $fields = array();

  // Add the fields defined by the User entity.
  foreach (field_info_instances('user', 'user') as $field_name => $instance) {
    $fields['field'][$field_name] = $instance['label'];
  }

  // Add properties.
  $wrapper = entity_metadata_wrapper('user');
  foreach ($wrapper->getPropertyInfo() as $name => $info) {
    // Don't allow any mapping to the uid or email field.
    if (in_array($name, array('uid', 'mail'))) {
      continue;
    }
    // Exclude properties that are arrays, e.g. "User roles".
    if (isset($info['type']) && strpos($info['type'], 'list') === 0) {
      continue;
    }
    // Fields have been dealt with above so exclude them here.
    if (!isset($info['field'])) {
      $fields['property'][$name] = $info['label'];
    }
  }
  return $fields;
}

/**
 * Construct an array of options for the Fullcube field dropdown.
 *
 * @see fullcube_field_settings_mapping()
 */
function fullcube_field_mapping_get_fullcube_field_options() {
  $options = array('' => '');
  $fields = db_query("SELECT fid, title FROM {fullcube_field_mapping_field}");
  while ($field = $fields->fetchObject()) {
    $options[$field->fid] = $field->title;
  }
  return $options;
}

/**
 * Returns the columns for a particular field.
 */
function _fullcube_field_mapping_get_columns($selected_field) {
  if (!empty($selected_field)) {
    $field = field_info_field($selected_field);
    if (isset($field['columns']) && !empty($field['columns'])) {
      return drupal_map_assoc(array_keys($field['columns']));
    }
  }
  return array('value' => 'value');
}

/**
 * Ajax callback for the column dropdown.
 */
function _fullcube_field_ajax_callback($form, $form_state) {
  $name = $form_state['triggering_element']['#name'];
  $matches = array();
  if (preg_match("/(\d+)\[field\]/", $name, $matches)) {
    $mid = $matches[1];
  }
  return $form[$mid]['column'];
}
