<?php

/**
 * @file
 * fullcube_webhook.webhook.inc
 */

/**
 * Listen to fullcube webhook message.
 */
function fullcube_webhook_message_callback() {

  // Do nothing if triggers are off. #TODO : Still pass ok response ?
  $webhook_enable = variable_get('fullcube_webhook_enable', 0);
  if (!$webhook_enable) {
    $response = array(
      'status' => 'ok',
      'messages' => array(),
    );
    return $response;
  }

  try {
    $payload = fullcube_webhook_payload();
    if (FALSE === $payload) {
      $message = t('Invalid JSON passed in payload');
      throw new \InvalidArgumentException($message);
    }
    elseif (!$payload) {
      watchdog('fullcube_webhook', 'No payload sent', array(), WATCHDOG_DEBUG);
      return array('status' => 'ok');
    }
    $args = func_get_args();
    $response = array(
      'status' => 'ok',
      'messages' => array(),
    );

    $event = NULL;
    if (!empty($payload['event'])) {
      $event = $payload['event'];
    }

    // Process the callback with event
    // Allow any module implementing hook_fullcube_intercept to receive
    // the response and act accordingly. They can also change the response built
    // so far.
    drupal_alter('fullcube_webhook_intercept', $event, $payload, $response);
  }
  catch (Exception $e) {
    watchdog_exception('fullcube_webhook', $e);
    $response = array(
      'status' => 'error',
      'messages' => $e->getMessage(),
    );
  }

  return $response;
}

/**
 * Returns the parsed payload.
 */
function fullcube_webhook_payload() {
  $data = file_get_contents('php://input');

  $payload = drupal_json_decode($data);

  // Log payload if debugging is enabled.
  $enable_debug = variable_get('fullcube_webhook_enable_debug', 0);
  if ($enable_debug) {
    watchdog('fullcube_webhook', 'Received %payload_event webhook event: %payload',
    array(
      '%payload_event' => $payload['event'],
      '%payload' => print_r($payload, TRUE),
    ), WATCHDOG_DEBUG);
  }

  // #TODO Compare if this is a valid request.
  if (!isset($_SERVER['HTTP_X_HOOK_SECRET'])) {
    throw new \Exception("HTTP header 'X-Hook-Secret' is missing.");
  }
  elseif (!extension_loaded('hash')) {
    throw new \Exception("Missing 'hash' extension to check the secret code validity.");
  }

  // Compare if this is a valid request.
  // $webhook_token = variable_get('fullcube_webhook_token', '');
  // $payloadId = (string) $payload['id'];
  // $secret_hook = hash_hmac('sha1', $payloadId, $webhook_token);
  // if ($secret_hook != $_SERVER['HTTP_X_HOOK_SECRET']) {
  //   throw new \Exception('Hook secret does not match. The hook secret calculated by Drupal is : ' . $secret_hook . ' and received is : ' . $_SERVER['HTTP_X_HOOK_SECRET']);
  // }

  return $payload;
}

/**
 * Delivery callback; Convets the array to JSON and returns.
 *
 * @param array $page_callback_result
 *   The result of a page callback. The array is simply converted to JSON, which
 *   is the rendered output of the page request.
 */
function fullcube_webhook_json_deliver(array $page_callback_result) {
  drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
  print drupal_json_encode($page_callback_result);
  module_invoke_all('exit');
  drupal_session_commit();
}
