<?php

/**
 * @file
 * Administration page callbacks for the fullcube_webhook module.
 */

/**
 * Fullcube webhook settings form.
 */
function fullcube_webhook_admin() {
  $form['fullcube_webhook_enable'] = array(
    '#type' => 'radios',
    '#title'  => t('Turn fullcube webhook triggers on or off'),
    '#default_value'  => variable_get('fullcube_webhook_enable', 0),
    '#options'  => array(1 => t('On'), 0 => t('Off')),
    '#description'  => t('Global level setting to turn on / off the fullcube webhook processor'),
  );

  $form['fullcube_webhook_token'] = array(
    '#title' => t('Security token'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('fullcube_webhook_token', ''),
    '#description'  => t('The security token provided by Fullcube or generated in your account.'),
  );

  $form['fullcube_webhook_enable_debug'] = array(
    '#type' => 'checkbox',
    '#title'  => t('Enable debugging.'),
    '#default_value'  => variable_get('fullcube_webhook_enable_debug', 0),
    '#description'  => t('Enabling debugging with log the webhook payload to watchdog.'),
  );
  return system_settings_form($form);
}
