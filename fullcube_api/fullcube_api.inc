<?php

/**
 * @file
 * Holds fucntion callbacks to request api.
 */

/**
 * Get user info.
 */
function fullcube_api_get_user_info() {
  $api_key = variable_get('fullcube_api_service_token', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new fullcubeProgram($fullcube_api);
  $user_info = $fullcube_person->getUserInfo($api_key);
  if (!empty($user_info)) {
    return $user_info;
  }
  return FALSE;
}

/**
 * Get program integration maps.
 */
function fullcube_api_get_program_integrations_map() {
  $api_key = variable_get('fullcube_api_service_token', '');
  $program_id = variable_get('fullcube_api_program_id', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_program = new FullcubeProgram($fullcube_api);
  $integrationsMap = $fullcube_program->integrationsMap($program_id, $api_key);
  if (empty($integrationsMap)) {
    return FALSE;
  }
  return $integrationsMap;
}

/**
 * Get program integration maps.
 */
function fullcube_api_get_programs_list() {
  $api_key = variable_get('fullcube_api_service_token', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_program = new FullcubeProgram($fullcube_api);
  $programList = $fullcube_program->programsList($api_key);
  if (empty($programList)) {
    return FALSE;
  }
  return $programList;
}

/**
 * Get user programs.
 */
function fullcube_api_get_user_programs() {
  $programs = array();
  $user_info = fullcube_api_get_user_info();

  if (!empty($user_info) && !empty($user_info['teams'])) {
    foreach ($user_info['teams'] as $pid => $team_data) {
      if (in_array(FULLCUBE_API_SYNCROLE, $team_data['assigned'])) {
        $programs[$pid] = $team_data['program']['name'];
      }
    }
  }
  return $programs;
}

/**
 * Get program campaigns.
 */
function fullcube_api_get_program_landing_pages() {
  $campaigns = array();
  $api_key = variable_get('fullcube_api_service_token', '');
  $program_id = variable_get('fullcube_api_program_id', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return $campaigns;
  }
  $fullcube_program = new FullcubeProgram($fullcube_api);
  $landingPages = $fullcube_program->landingPages($program_id, $api_key);
  return $landingPages;
}

/**
 * Get program membership levels.
 */
function fullcube_api_get_program_membership_levels() {
  $api_key = variable_get('fullcube_api_service_token', '');
  $program_id = variable_get('fullcube_api_program_id', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }
  $fullcube_program = new FullcubeProgram($fullcube_api);
  $membershipLevels = $fullcube_program->membershipLevels($program_id, $api_key);
  if (empty($membershipLevels)) {
    return FALSE;
  }
  return $membershipLevels;
}

/**
 * Link Identity.
 */
function fullcube_api_link_identity($params) {
  if (empty($params['uid']) || empty($params['id'])) {
    return FALSE;
  }
  $api_key = variable_get('fullcube_api_service_token', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new FullcubePerson($fullcube_api);
  $link_identity = $fullcube_person->linkIdenttity($params['id'], 'drupal', $params['uid'], $api_key);
  if (!$link_identity) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Delete Identity.
 */
function fullcube_api_delete_identity($params) {
  if (empty($params['uid']) || empty($params['id'])) {
    return FALSE;
  }
  $api_key = variable_get('fullcube_api_service_token', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new FullcubePerson($fullcube_api);
  $delete_identity = $fullcube_person->deleteIdenttity($params['id'], 'drupal', $params['uid'], $api_key);
  if (!$delete_identity) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Delete Identity.
 */
function fullcube_api_update_person($personId, $data) {
  if (empty($personId) || empty($data)) {
    return FALSE;
  }
  $api_key = variable_get('fullcube_api_service_token', '');
  $program_id = variable_get('fullcube_api_program_id', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new FullcubePerson($fullcube_api);
  $update_person = $fullcube_person->updatePerson($personId, $program_id, $data, $api_key);
  if (!$update_person) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Create Prospect.
 */
function fullcube_api_create_prospect($data) {
  if (empty($data)) {
    return FALSE;
  }
  $api_key = variable_get('fullcube_api_service_token', '');
  $campaignId = variable_get('fullcube_api_landing_page', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new FullcubePerson($fullcube_api);
  $prospect_info = $fullcube_person->addProspect($campaignId, $data, $api_key);
  if (!$prospect_info) {
    return FALSE;
  }
  return $prospect_info;
}

/**
 * Create member.
 */
function fullcube_api_add_member($data) {
  if (empty($data)) {
    return FALSE;
  }
  $api_key = variable_get('fullcube_api_service_token', '');
  $campaignId = variable_get('fullcube_api_landing_page', '');

  $fullcube_api = fullcube_get_api_object();
  if (empty($fullcube_api)) {
    return FALSE;
  }

  $fullcube_person = new FullcubePerson($fullcube_api);
  $prospect_info = $fullcube_person->addMember($campaignId, $data, $api_key);
  if (!$prospect_info) {
    return FALSE;
  }
  return $prospect_info;
}

/**
 * Get person info.
 */
function fullcube_api_get_person_info($personId) {
  $api_key = variable_get('fullcube_api_service_token', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($personId)) {
   return FALSE;
  }
  $fullcube_person = new FullcubePerson($fullcube_api);
  $person_info = $fullcube_person->getPerson($personId, $api_key);
  return $person_info;
}

/**
 * Get person currentMembership.
 */
function fullcube_api_get_person_current_membership($personId) {
  $api_key = variable_get('fullcube_api_service_token', '');
  $fullcube_api = fullcube_get_api_object();
  if (empty($personId)) {
   return FALSE;
  }
  $fullcube_person = new FullcubePerson($fullcube_api);
  $person_membership = $fullcube_person->getPersonCurrentMembership($personId, $api_key);
  return $person_membership;
}
