/**
 * @file
 * FormAPI integration with the Recurly forms.
 */
(function ($) {

Drupal.fullcube = Drupal.fullcube || {};

Drupal.behaviors.fullcubeUpdateBillingForm = {
  attach: function (context, settings) {
    $('#fullcube-api-update-billing').once('fullcube-api-update-billing', function () {
      $(this).bind('submit', Drupal.fullcube.recurlyJSTokenFormSubmit);
    });
  }
};


/**
 * Handles submission of the subscribe form.
 */
Drupal.fullcube.recurlyJSTokenFormSubmit = function(event) {
  event.preventDefault();
  // Reset the errors display
  $('#recurly-form-errors').html('');
  $('input').removeClass('error');

  // Disable the submit button
  $('button').attr('disabled', true);

  var form = this;
  recurly.token(form, function (err, token) {
    if (err) {
      Drupal.fullcube.recurlyJSFormError(err);
    }
    else {
      form.submit();
    }
  });
};

/**
 * Configures form for recurly hosted fields.
 */
Drupal.behaviors.recurlyJSConfigureForm = {
  attach: function (context, settings) {
    recurly.configure({
      publicKey: settings.fullcube_public_key,
      style: {
        number: {
          placeholder: 'Credit Card Number',
        },
        month: {
          placeholder: 'Month (mm)',
        },
        year: {
          placeholder: 'Year (yyyy)',
        },
        cvv: {
          placeholder: 'Security Code',
        },
      }
    });
  }
};

/**
* Handles form errors.
*/
Drupal.fullcube.recurlyJSFormError = function(err) {
  $('button').attr('disabled', false);

  // Add the error class to all form elements that returned an error.
  if (typeof err.fields !== 'undefined') {
    $.each(err.fields, function (index, value) {
      $('input[data-recurly="' + value + '"]').addClass('error');
    });
  }

  // Add the error message to the form within standard Drupal message markup.
  if (typeof err.message !== 'undefined') {
    var messageMarkup = '<div class="messages error">' + err.message + '</div>';
    $('#recurly-form-errors').html(messageMarkup);
  }
};

})(jQuery);
