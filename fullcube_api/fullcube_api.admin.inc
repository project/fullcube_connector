<?php

/**
 * @file
 * Administration page callbacks for the fullcube_api module.
 */

/**
 * Fullcube Api settings form.
 */
function fullcube_api_settings() {
  $form['fullcube_api_service_url'] = array(
    '#title' => t('Service URL'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('fullcube_api_service_url', 'https://api.fullcube.io/api/v1/'),
    '#description'  => t('The service url for accessing api.'),
  );

  $form['fullcube_api_service_token'] = array(
    '#title' => t('Service API token'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('fullcube_api_service_token', ''),
    '#description'  => t('The security token provided by Fullcube or generated in your account for accessing API.'),
  );

  $form['fullcube_api_program_id'] = array(
    '#title' => t('Fullcube Program'),
    '#type' => 'select',
    "#empty_option" => t('- Select -'),
    '#options' => fullcube_api_get_user_programs(),
    '#default_value' => variable_get('fullcube_api_program_id', ''),
    '#description'  => t('The fullcube program associated with website.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="type"]' => array('value' => 0),
      ),
    ),
  );

  $form['fullcube_api_recurly_public_key'] = array(
    '#type' => 'textfield',
    '#title'  => t('Recuryly Public Key.'),
    '#disabled'  => TRUE,
    '#default_value'  => variable_get('fullcube_api_recurly_public_key', ''),
    '#description'  => t('Auto captured via api on saving api token and program ID.'),
    '#states' => array(
      'visible' => array(
        ':input[name="fullcube_api_program_id"]' => array('!value' => ''),
      ),
    ),
  );

  $form['fullcube_api_landing_page'] = array(
    '#title' => t('Program Campaign'),
    '#type' => 'select',
    "#empty_option" => t('- Select -'),
    '#options' => fullcube_api_get_program_landing_pages(),
    '#default_value' => variable_get('fullcube_api_landing_page', ''),
    '#description'  => t('The fullcube landing page associated with program. This list is refreshed after changing program'),
    '#states' => array(
      'visible' => array(
        ':input[name="fullcube_api_program_id"]' => array('!value' => ''),
      ),
      'required' => array(
        ':input[name="fullcube_api_user_add_member"]' => array('checked' => TRUE),
      ),
    ),
  );

  if (module_exists('fullcube_field_mapping')) {
    $form['fullcube_api_user_register_prospect'] = array(
      '#type' => 'checkbox',
      '#title'  => t('Enable program prospect creation on user registration.'),
      '#default_value'  => variable_get('fullcube_api_user_register_prospect', 0),
      '#states' => array(
        'visible' => array(
          ':input[name="fullcube_api_program_id"]' => array('!value' => ''),
        ),
      ),
    );

    $form['fullcube_api_user_add_prospect'] = array(
      '#type' => 'checkbox',
      '#title'  => t('Enable program prospect creation on user creation from backend.'),
      '#default_value'  => variable_get('fullcube_api_user_add_prospect', 0),
      '#states' => array(
        'visible' => array(
          ':input[name="fullcube_api_program_id"]' => array('!value' => ''),
        ),
      ),
    );

    $form['fullcube_api_user_add_member'] = array(
      '#type' => 'checkbox',
      '#title'  => t('Add user as program member if the credit card fields are
      added and recurly validation is done.'),
      '#default_value'  => variable_get('fullcube_api_user_add_member', 0),
      '#states' => array(
        'visible' => array(
          ':input[name="fullcube_api_program_id"]' => array('!value' => ''),
        ),
      ),
    );
  }

  $form['fullcube_api_enable_debug'] = array(
    '#type' => 'checkbox',
    '#title'  => t('Enable debugging.'),
    '#default_value'  => variable_get('fullcube_api_enable_debug', 0),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'fullcube_api_settings_form_submit';
  return $form;
}

/**
 * Submit handler to fetch and Save the FC API Settings.
 */
function fullcube_api_settings_form_submit($form, &$form_state) {
  $integrationsmap = fullcube_api_get_program_integrations_map();
  if (!empty($integrationsmap['recurly'])) {
    $recurly_publickey = $integrationsmap['recurly']['publicKey'];
    variable_set('fullcube_api_recurly_public_key', $recurly_publickey);
  }
}

/**
 * Form callback; Display the form for updating billing information.
 */
function fullcube_api_update_billing($form, $form_state) {
  // Includes iframe styling.
  drupal_add_css(drupal_get_path('module', 'fullcube_api') . '/css/fillcube_api_recurlyjs.css');
  $user_id = arg(1);
  $user = user_load($user_id);

  $form['#prefix'] = '<div class="recurly-form-wrapper">';
  $form['#suffix'] = '</div>';

  // recurly-element.js adds errors here upon failed validation.
  $form['errors'] = array(
    '#markup' => '<div id="recurly-form-errors"></div>',
    '#weight' => -300,
  );

  // Initialize the Recurly client with the site-wide settings.
  // Add and initialize Recurly.js.
  $recurly_publicKey = variable_get('fullcube_api_recurly_public_key', '');
  if (empty($recurly_publicKey)) {
    form_set_error('', t('Unable to initialize billing update process. Please check after sometime.'));
    return $form;
  }

  $defaults = array();
  $user_wrapper = entity_metadata_wrapper('user', $user);
  $uuid_field = _fullcube_field_retrieve_uuid_field();
  // Pass the recurly data to FC.
  $personId = $user_wrapper->{$uuid_field}->value();
  $field_mapping = _fullcube_field_mapping_retrieve();
  foreach ($field_mapping as $mapping) {
    if (in_array($mapping['fcpath'],
      array(
        'firstName',
        'lastName',
        'billingInfo.ccYear',
        'billingInfo.ccMonth',
        'billingInfo.cc_number',
      ))
    ) {
      $defaults[$mapping['fcpath']] = $user_wrapper->{$mapping['field']}->value();
    }
  }

  // #TODO hide for now.
  $form['first_name'] = array(
    '#type' => 'hidden',
    '#size' => '30',
    '#attributes' => array('id' => 'fullcube-api-recurly-first-name', 'data-recurly' => 'first_name'),
    '#value' => $defaults['firstName'],
    '#weight' => -60,
  );

  $form['last_name'] = array(
    '#type' => 'hidden',
    '#size' => '30',
    '#attributes' => array('id' => 'fullcube-api-recurly-last-name', 'data-recurly' => 'last_name'),
    '#value' => $defaults['lastName'],
    '#weight' => -59,
  );

  // Fetch the current default values.
  $form['existing'] = array(
    '#theme' => 'fullcube_api_credit_card_information',
    '#first_name' => $defaults['firstName'],
    '#last_name' => $defaults['lastName'],
    '#cc_year' => $defaults['billingInfo.ccYear'],
    '#cc_month' => $defaults['billingInfo.ccMonth'],
    '#cc_number' => $defaults['billingInfo.cc_number'],
  );

  $form['number'] = array(
    '#title' => t('Card Number'),
    '#markup' => '<div data-recurly="number"></div>',
    '#weight' => -55,
  );

  $form['month'] = array(
    '#title' => t('MM'),
    '#markup' => '<div data-recurly="month"></div>',
    '#weight' => -50,
  );
  $form['year'] = array(
    '#title' => t('YYYY'),
    '#markup' => '<div data-recurly="year"></div>',
    '#weight' => -45,
  );

  $form['cvv'] = array(
    '#title' => t('CVC'),
    '#markup' => '<div data-recurly="cvv"></div>',
    '#weight' => -44,
  );
  $form['recurly-token'] = array(
    '#type' => 'hidden',
    '#attributes' => array(
      'data-recurly' => 'token',
    ),
  );
  $form['person-id'] = array(
    '#type' => 'hidden',
    '#value' => $personId,
  );

  _fullcube_api_recurlyjs_form_attach_js($form);

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('_fullcube_api_update_billing_submit'),
  );
  return $form;
}

/**
 * Submit handler for recurlyjs_update_billing_form().
 */
function _fullcube_api_update_billing_submit($form, $form_state) {
  $recurly_token = isset($form_state['values']['recurly-token']) ? $form_state['values']['recurly-token'] : NULL;
  $person_id = $form_state['values']['person-id'];
  if (!empty($recurly_token) && !empty($person_id)) {
    $data = array();
    $data['id'] = $person_id;
    $data['billingInfo'] = array(
      'billingToken' => $recurly_token,
    );
    $person_update = fullcube_api_update_person($person_id, $data);
    if (!$person_update) {
      watchdog('fullcube_webhook_connector', 'Error updating billing info with  Person ID %personID : %data',
      array('personID' => $person_id, '%data' => print_r($data, TRUE)), WATCHDOG_DEBUG);
      drupal_set_message(t('There was error updating your billing information. Please try after some time.'), 'error');
    }
    else {
      $enable_debug = variable_get('fullcube_api_enable_debug', 0);
      if ($enable_debug) {
        watchdog('fullcube_api', 'Sent the recurly token info to FC for person ID %personID : %data',
        array('%personID' => $personID, '%data' => print_r($data, TRUE)), WATCHDOG_DEBUG);
      }
      drupal_set_message(t('Billing information updated successfully. It might take some time to reflect your current billing information below.'));
    }
  }
}

/**
 * Attach Recurly JS library to Drupal form.
 *
 * @param array &$form
 *   A Drupal form array.
 */
function _fullcube_api_recurlyjs_form_attach_js(&$form) {
  $recurly_public_key = variable_get('fullcube_api_recurly_public_key', '');
  // Add Recurly.js and the inline code to the page.
  $form['#attached']['js'][] = 'https://js.recurly.com/v4/recurly.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'fullcube_api') . '/fullcube-api-element.js';
  $form['#attached']['js'][] = array(
    'data' => array('fullcube_public_key' => $recurly_public_key),
    'type' => 'setting',
  );
}
