<?php

/**
 * @file
 * FullcubeProgram class.
 */

 /**
  * Construct class.
  */

/**
 * Class FullcubePerson.
 */
class FullcubePerson {

  /**
   * Construct.
   */
  public function __construct(Fullcube $master) {
    $this->master = $master;
  }

  /**
   * Update Person.
   */
  public function updatePerson($personId, $programID, $data, $access_token) {
    // #TODO Uncomment below once person ca be udpated via program ID endpoint
    // For more details, see #128131859.
    /* return $this->master->put('programs/' . $programID . '/people/' .
    $personId . '?access_token=' . $access_token, $data); */
    return $this->master->put('people/' . $personId . '?access_token=' .
    $access_token, $data);
  }

  /**
   * Add prospect.
   */
  public function addProspect($campaignId, $data, $access_token) {
    return $this->master->post('landingPages/' . $campaignId . '/addProspect?access_token=' . $access_token, $data);
  }

  /**
   * Add member.
   */
  public function addMember($campaignId, $data, $access_token) {
    return $this->master->post('landingPages/' . $campaignId . '/addMember?access_token=' . $access_token, $data);
  }

  /**
   * Notify Fullcube about linked account.
   */
  public function linkIdenttity($personId, $appName, $userId, $access_token) {
    $_params = array(
      "id" => $personId,
      "provider" => $appName,
      "externalId" => $userId,
    );
    return $this->master->post('people/' . $personId . '/identity?access_token=' . $access_token, $_params);
  }

  /**
   * Update a person membership to new plan
   */
  public function updateMembership($personId, $planId, $access_token) {
    $_params = array(
      'planId' => $planId,
    );
    return $this->master->post('people/' . $personId . '/updateMembership?access_token=' . $access_token, $_params);
  }

  /**
   * Close Person and terminate membership
   */
  public function closePerson($personId, $refund = 'none', $access_token) {
    $_params = array(
      "id" => $personId,
      "refund" => $refund,
    );
    return $this->master->put('people/' . $personId . '/close?access_token=' . $access_token, $_params);
  }

  /**
   * Cancel a Person's membership
   */
  public function cancelMembershiip($personId, $access_token) {
    return $this->master->put('people/' . $personId . '/cancel?access_token=' . $access_token);
  }

  /**
   * Schedule a pause for person membership
   */
  public function pauseMembershiip($personId, $pauseCycles, $access_token) {
    $_params = array(
      "id" => $personId,
      "pauseCycles" => $pauseCycles,
    );
    return $this->master->post('people/' . $personId . '/pauseMembership?access_token=' . $access_token, $_params);
  }

  /**
   * Resume a paused person membership
   */
  public function resumeMembershiip($personId, $pauseCycles, $access_token) {
    $_params = array(
      "id" => $personId,
    );
    return $this->master->post('people/' . $personId . '/resumeMembership?access_token=' . $access_token, $_params);
  }

  /**
   * Notify Fullcube about linked account.
   */
  public function deleteIdenttity($personId, $appName, $userId, $access_token) {
    return $this->master->delete('people/' . $personId . '/identity?provider=' .
    $appName . '&externalId=' . $userId . '&access_token=' . $access_token);
  }

  /**
   * Get Person Info.
   */
  public function getPerson($peopleId, $access_token) {
    return $this->master->get('People/' . $peopleId . '?access_token=' . $access_token);
  }

  /**
   * Get Person Current Membership.
   */
  public function getPersonCurrentMembership($peopleId, $access_token) {
    return $this->master->get('People/' . $peopleId . '/currentMembership?access_token=' . $access_token);
  }

  /**
   * Get Person Fulfillments.
   */
  public function getPersonFulfillments($peopleId, $access_token) {
    return $this->master->get('People/' . $peopleId . '/fulfillments?access_token=' . $access_token);
  }
}
