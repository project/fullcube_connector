<?php

/**
 * @file
 * FullcubeProgram class.
 */

 /**
  * Class FullcubeProgram.
  */
class FullcubeProgram {

  /**
   * Construct.
   */
  public function __construct(Fullcube $master) {
    $this->master = $master;
  }

  /**
   * Get Program integrations Map.
   */
  public function integrationsMap($programId, $access_token) {
    return $this->master->get('Programs/' . $programId . '/integrationsMap' . '?access_token=' . $access_token);
  }

  /**
   * Get user info based on access token integrations Map.
   */
  public function getUserInfo($access_token) {
    $user_info = $this->master->get('users/me/info' . '?access_token=' . $access_token);
    return $user_info;
  }

  /**
   * Get Program list.
   */
  public function programsList($access_token) {
    $programs = $this->master->get('/Programs?access_token=' . $access_token);
    if (!empty($programs)) {
      $programlist = array();
      foreach ($programs as $program) {
        $programlist[$program['id']] = $program['name'];
      }
      return $programlist;
    }
    else {
      return array();
    }
  }

  /**
   * Get landingPages list.
   */
  public function landingPages($programId, $access_token) {
    $landingPageslist = array();
    $landingPages = $this->master->get('/Programs/' . $programId . '/landingPages?access_token=' . $access_token);

    if (!empty($landingPages)) {
      foreach ($landingPages as $landingPage) {
        $landingPageslist[$landingPage['id']] = $landingPage['name'];
      }
    }
    return $landingPageslist;
  }

  /**
   * Get Program Membership levels.
   */
  public function membershipLevels($programId, $access_token) {
    return $this->master->get('Programs/' . $programId . '/membershipLevels' . '?access_token=' . $access_token);
  }

  /**
   * Get Program Plans.
   */
  public function plans($programId, $access_token, $externalId = null) {
    $planrequest = 'Programs/' . $programId . '/plans' . '?access_token=' . $access_token;
    if (!empty($externalId)) {
      $filter = array (
        'where' => array (
          'externalId' => $externalId,
        )
      );
      $planrequest .= '&filter=' . json_encode($filter, JSON_FORCE_OBJECT);
    }
    return $this->master->get($planrequest);
  }

}
