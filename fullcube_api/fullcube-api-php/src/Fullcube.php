<?php

/**
 * @file
 * API client for Fullcube API.
 */

require_once 'Fullcube/People.php';
require_once 'Fullcube/Program.php';

/**
 * Class.
 */
class Fullcube {

  public $apikey;
  public $ch;
  public $root;
  public $debug = FALSE;

  /**
   * Construct the class.
   */
  public function __construct($apikey = NULL, $opts = array()) {

    if (!$apikey) {
      throw new Exception('You must provide a Fullcube API key');
    }

    $this->apikey = $apikey;

    // Currently get the root from variable.
    $root = variable_get('fullcube_api_service_url', 'https://develop.fullcube.io/api/v1/');

    $this->root = rtrim($root, '/') . '/';

    if (!isset($opts['timeout']) || !is_int($opts['timeout'])) {
      $opts['timeout'] = 600;
    }

    if (isset($opts['debug'])) {
      $this->debug = TRUE;
    }

    $this->ch = curl_init();

    if (isset($opts['CURLOPT_FOLLOWLOCATION']) && $opts['CURLOPT_FOLLOWLOCATION'] === TRUE) {
      curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, TRUE);
    }

    curl_setopt($this->ch, CURLOPT_USERAGENT, 'Fullcube-PHP/1.0');
    curl_setopt($this->ch, CURLOPT_HEADER, FALSE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($this->ch, CURLOPT_TIMEOUT, $opts['timeout']);

    // Parse http auth from url.
    $http_auth = parse_url($root);
    if (!empty($http_auth['user']) && !empty($http_auth['pass'])) {
      curl_setopt($this->ch, CURLOPT_USERPWD, $http_auth['user'] . ':' . $http_auth['pass']);
    }

    $this->person = new FullcubePerson($this);
    $this->program = new FullcubeProgram($this);
  }

  /**
   * Destruct.
   */
  public function __destruct() {
    if (is_resource($this->ch)) {
      curl_close($this->ch);
    }
  }

  /**
   * Get.
   */
  public function get($url) {
    $ch     = $this->ch;
    curl_setopt($ch, CURLOPT_URL, $this->root . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response_body = curl_exec($ch);
    $result = json_decode($response_body, TRUE);
    $info = curl_getinfo($ch);

    if (!empty($result['error']) &&  !empty($result['error']['message'])) {
      $this->castError($result['error']['message']);
      return FALSE;
    }

    return $result;
  }

  /**
   * Delete.
   */
  public function delete($url) {
    $ch     = $this->ch;
    curl_setopt($ch, CURLOPT_URL, $this->root . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response_body = curl_exec($ch);
    $result = json_decode($response_body, TRUE);
    $info = curl_getinfo($ch);

    if (!empty($result['error']) &&  !empty($result['error']['message'])) {
      $this->castError($result['error']['message']);
      return FALSE;
    }

    return $result;
  }

  /**
   * Put.
   */
  public function put($url, $params) {
    $params = json_encode($params);
    $ch     = $this->ch;
    curl_setopt($ch, CURLOPT_URL, $this->root . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
    ));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response_body = curl_exec($ch);
    $result = json_decode($response_body, TRUE);
    $info = curl_getinfo($ch);

    if (!empty($result['error']) &&  !empty($result['error']['message'])) {
      $this->castError($result['error']['message']);
      return FALSE;
    }
    return $result;
  }

  /**
   * POST.
   */
  public function post($url, $params) {
    $params = json_encode($params);
    $ch     = $this->ch;
    curl_setopt($ch, CURLOPT_URL, $this->root . $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($this->ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);

    $start = microtime(TRUE);
    $this->log('Call to ' . $this->root . $url . ': ' . $params);
    if ($this->debug) {
      $curl_buffer = fopen('php://memory', 'w+');
      curl_setopt($ch, CURLOPT_STDERR, $curl_buffer);
    }

    $response_body = curl_exec($ch);

    $info = curl_getinfo($ch);
    $time = microtime(TRUE) - $start;
    if ($this->debug) {
      rewind($curl_buffer);
      $this->log(stream_get_contents($curl_buffer));
      fclose($curl_buffer);
    }
    $this->log('Completed in ' . number_format($time * 1000, 2) . 'ms');
    $this->log('Got response: ' . $response_body);
    $result = json_decode($response_body, TRUE);
    if (!empty($result['error']) &&  !empty($result['error']['message'])) {
      $this->castError($result['error']['message']);
      return FALSE;
    }

    return $result;
  }

  /**
   * Cast Error.
   */
  public function castError($message) {
    // Lets log response error to watchdog.
    watchdog('fullcube_api', 'Fullcube API ERROR :  %error_message.', array('%error_message' => $message), WATCHDOG_DEBUG);
  }

  /**
   * Log.
   */
  public function log($msg) {
    if ($this->debug) {
      error_log($msg);
    }
  }

}
