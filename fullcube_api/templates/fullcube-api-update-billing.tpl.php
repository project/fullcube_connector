<?php
/**
 * @file
 * Displays the form for updating a credit card number.
 */
?>

<?php if (isset($form['existing']['#cc_number'])): ?>
  <p><?php print t('We currently have the following credit card on file:'); ?></p>
  <p><?php print drupal_render($form['existing']); ?></p>
  <p><?php print t('To change your credit card, update your information below:'); ?></p>
<?php else: ?>
  <p><?php print t('No credit card currently on file.'); ?></p>
<?php endif; ?>
<?php
  if(!empty($form['existing'])) {
    unset($form['existing']);
  }
?>
<?php print drupal_render_children($form); ?>
