<?php
/**
 * @file
 * Prints out a summary of an existing credit card with masked digits.
 */
?>
<div class="credit-card-information">
  <div class="credit-card-name"><?php print $first_name . ' ' . $last_name; ?></div>
  <div class="credit-card-date"><?php print 'Exp: ' . $cc_month . '/' . $cc_year; ?></div>
  <div class="credit-card-number"><?php print $cc_number; ?></div>
</div>
