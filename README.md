# Drupal Fullcube connector

This module provides integration with Fullcube. The Fullcube membership management platform enables member-based businesses with tools to acquire more members at optimized CPA’s, establish a credit card-on-file relationship with their members, engage and retain members through messaging automation, and deliver member benefits to a native mobile app and existing websites.

* [Overview](#overview)
  * [Installation](#installation)
  * [Fullcube API](#fullcube-api)
  * [Fullcube Field Mapping](#fullcube-field-mapping)
  * [Fullcube Webhook](#fullcube-webhook)
  * [Fullcube Webhook Connector](#fullcube-webhook-connector)

## Overview

This package contains 4 modules:

1. **Fullcube API** (fullcube_api)
   API connector to fullcube platform.

2. **Fullcube Field Mapping** (fullcube_field_mapping)
   Map fields from the Drupal user entity to fullcube person object.

3. **Fullcube Webhook** (fullcube_webhook)
   Webhooks listener for the fullcube platform.

4. **Fullcube Webhook Connector** (fullcube_webhook_connector)
   Acts on webhooks received from the fullcube platform (keeps data in sync).

### Installation

1. Copy the drupal-fullcube-connector directory to the modules folder in your installation.

2. Enable any of the module as per your needs using Administer -> Modules (/admin/modules)

---

## Fullcube API

The API module is a helper module to connect to Fullcube API. The module exposes api to get list of programs, add/update people/membership, and link/unlink drupal identity to person.

### Configuration

After installation you can visit: /admin/config/fullcube/api for module configuration. The available options are:

1. **Service URL:**
   This is the API end point to which you will connect to access your program data. Usually: https://api.fullcube.io/api/v1, for development server it would be https://api-develop.fullcube.io/api/v1. You can contact your Account manager at fullcube for the correct url.

2. **Service API Token:**
   This will be provided by your account manager at fullcube.

3. **Program:**
   When you save the access API token, you will be able to select the program from a list of those to which you have access. The selected program will be configure for use with the drupal site.

4. **Recurly Public Key:**
   The recurly key is automatically fetched depending on the program selected.

5. **Program campaign:**
   When a new user is created in drupal and pushed to the fullcube platform, it is added as a prospect. But if the user has linked their credit card to their profile, then based on your settings, it can directly create a member using the workflows of the campaign you select here.

6. **Prospect creation from registration form:**
   If this setting is checked, then when a new user is created via /user/register in drupal, it will add that person as a prospect in fullcube. _(Depends on fullcube_field_mapping)_

7. **Prospect creation from add user form:**
   If this setting is checked then when a new user is created via /admin/people/create in drupal, it will add that person as a prospect in fullcube. _(Depends on fullcube_field_mapping)_

8. **Program member**:
   If this setting is checked and a user has configured credit card in his account, when a new user is created via /admin/people/create in drupal, it will add that person as a prospect in fullcube. _(Depends on fullcube_field_mapping)_

---

## Fullcube Field Mapping

The fullcube field mapping module helps you to map drupal user entity fields with fullcube person attributes.

### Configuration

After installation you can visit: /admin/config/fullcube/mapping for mapping the fields. The required field mapping fields are email, first name, last name, psm uuid.

When Credit card fields are mapped, recurly js is embedded into the drupal "user registration" and "user add" forms and a recurly token is captured on submission and passed along with user data to the fullcube platform.

To add new fullcube fields, you can visit: /admin/config/fullcube/fields, which will then be available in mapping. Each field can be mapped only once.
For capturing any custom data for member, you can add new fullcube fields with "preferences" as prefix like: preferences.favourite_color, and map that with drupal fields under mapping. Data from user profile for those preferences fields will be passed to fullcube as key | value data and available on member profile screen under preferences.

---

## Fullcube Webhook

Fullcube Webhook is a helper module to listen to and act on fullcube webhooks.

The end point for receiving webhooks is: <yourwebsite>/fullcube/message

### Configuration

After installation you can visit: /admin/config/fullcube/webhook to configure the webhooks credentials. The available config fields are:

1. **Enable / Disable Webhook:**
   You can turn on/off functionality to listening to webhooks.

2. **Webhook Secret:**
   This will be provided to you by Account manager at fullcube platform.

3. **Debug:**
   Enabling this will log the messages and payload data coming via webhooks.

---

## Fullcube Webhook Connector

Fullcube Webhook Connector modules extends the functionality of webhook module and acts on the incoming webhook data to create / update users.

Below are available webhooks which are processed:

1. **Person.created:**
   Created a user in drupal and links back the drupal user ID to the person in the fullcube platform.

2. **Person.canceled:**
   Updates a user info when canceled webhook is received.

3. **Person.updated:**
   Updates a user in drupal based on the changes made in the fullcube platform.

4. **Person.subscribed:**
   Assigns user a specific role as per config and as per the persons membership level.

5. **Person.lapsed:**
   Blocks/ Deletes a user as per config.

6. **Person.doPasswordReset:**
   Triggers a password reset from drupal for the user linked with person payload

7. **Person.doWelcomeEmail:**
   Triggers welcome email (user register) from drupal for the user linked with person payload.
