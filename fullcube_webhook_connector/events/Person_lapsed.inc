<?php

/**
 * @file
 * Code for the fullcube webhook Person.lapsed event processing.
 */

/**
 * Callback for processing Person.lapsed webhook.
 */
function fullcube_webhook_connector_process_person_lapsed($payload) {
  if (!$payload['data']) {
    return FLASE;
  }
  $data  = $payload['data']['person'];

  // Get the user identity and trigger a password reset request.
  $email = $data['email'];

  // Check the status of user lapsed action in config.
  $user_lapsed_action = $webhook_token = variable_get('fullcube_webhook_connector_lapsed_action', 'user_delete');
  $account = user_load_by_mail($email);
  if (isset($account->uid)) {
    switch ($user_lapsed_action) {
      case 'user_delete':
        // Delete the user.
        user_delete($account->uid);
        break;

      case 'user_block':
        // Block the user.
        user_save($account, array('status' => 0));
        break;

    }
  }
  return TRUE;
}
