<?php

/**
 * @file
 * Code for the fullcube webhook Person.doPasswordReset event processing.
 */

/**
 * Callback for processing Person.doPasswordReset webhook.
 */
function fullcube_webhook_connector_process_person_dopasswordreset($payload) {
  if (!$payload['data']) {
    return FLASE;
  }
  $data  = $payload['data']['person']['identity'];

  // Get the user identity and trigger a password reset request.
  if (!empty($data['externalId']) && isset($data['provider']) && $data['provider'] == 'drupal') {
    $uid = $data['externalId'];

    // Load a user.
    $account = user_load($uid);
    if (isset($account->uid)) {
      // Invoke the email.
      _user_mail_notify('password_reset', $account);
    }
  }
  return TRUE;
}
