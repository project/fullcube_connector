<?php

/**
 * @file
 * Code for the fullcube webhook Person.create event processing.
 */

/**
 * Callback for processing Person.create webhook.
 */
function fullcube_webhook_connector_process_person_created($payload, $uid = FALSE) {

  if (!$payload['data']) {
    return FLASE;
  }
  $data  = $payload['data']['person'];
  // Dont create a person if payload does not contains the membership infor.
  $current_membership = !empty($data['currentMembership']) ? $data['currentMembership'] : FALSE;
  if (!$current_membership || !in_array($current_membership['status'], array('active', 'provisional'))) {
    return FALSE;
  }

  // Set the next billing date param which is mapped with drupal date field.
  if (isset($current_membership['currentSubscription']) && !empty($current_membership['currentSubscription']['currentPeriodEndDate'])) {
    $data['currentPeriodEndDate'] = $current_membership['currentSubscription']['currentPeriodEndDate'];
  }

  if (isset($current_membership['currentSubscription']) && !empty($current_membership['currentSubscription']['plan'])) {
    $data['currentSubscriptionPlanName'] =
      $current_membership['currentSubscription']['plan']['name'] ? $current_membership['currentSubscription']['plan']['name'] : '';
    $data['currentSubscriptionPlanCode'] =
      $current_membership['currentSubscription']['plan']['code'] ? $current_membership['currentSubscription']['plan']['code'] : '';
  }

  // Provide Membershiplevel data for mapping
  if (isset($current_membership['membershipLevel'])) {
    $data['currentMembershipMembershipLevelCode'] =
      $current_membership['membershipLevel']['code'] ? $current_membership['membershipLevel']['code'] : '';
    $data['currentMembershipMembershipLevelName'] =
      $current_membership['membershipLevel']['name'] ? $current_membership['membershipLevel']['name'] : '';
  }

  // Create a new user if does not already exists, update if exists.
  // Currently matching for existing user based on email address.
  $email = $data['email'];

  // Get roles applicable for current membership.
  $roles = array();
  $user_roles = user_roles();
  $membership_code = isset($current_membership['membershipLevel']['code']) ? $current_membership['membershipLevel']['code'] : FALSE;
  $membership_roles = variable_get('fullcube_webhook_membership_' . $membership_code . '_roles', array());

  // Make sure the mebership level is not skipped from account creation / updation
  $membershiplevels_account = variable_get('fullcube_webhook_membership_levels_account', null);
  if (!empty($membershiplevels_account) && !in_array($membership_code, $membershiplevels_account)) {
    return FALSE;
  }

  foreach ($membership_roles as $rid) {
    $roles[$rid] = $user_roles[$rid];
  }

  // Make sure authenticated role is always there.
  if (!isset($roles[DRUPAL_AUTHENTICATED_RID])) {
    $roles[DRUPAL_AUTHENTICATED_RID] = 'authenticated user';
  }

  // Get the configured user points.
  $user_points = variable_get('fullcube_webhook_membership_' . $membership_code . '_userpoints', 0);

  if ((bool) db_select('users')->fields('users', array('uid'))->condition('mail', db_like($email), 'LIKE')->range(0, 1)->execute()->fetchField() || !empty($uid)) {
    // Account already exists so lets update and link the user identity to FC.
    // Currently matching based on UID.
    // Get the user with email ID
    // UPDATE USER.
    if (!empty($uid)) {
      $account = user_load($uid);
    }
    else {
      $account = user_load_by_mail($email);
    }

    // Map email also.
    $account->mail = $email;

    $fields = array();
    $link_identity = FALSE;
    // Map the fields specified for mapping.
    if (module_exists('fullcube_field_mapping')) {
      // Get the field mapping.
      $field_mapping = _fullcube_field_mapping_retrieve('profile');
      if (!empty($field_mapping)) {
        foreach ($field_mapping as $value) {
          $query = db_select('fullcube_field_mapping_field', 'wm')
            ->fields('wm', array('path'))
            ->condition('wm.fid', $value['fid'])
            ->range(0, 1)
            ->execute();
          $path_result = $query->fetchObject();
          $path = $path_result->path;

          // Check if the path contains any .
          $preferences_field = explode('.', $path);
          if (count($preferences_field) > 1 && isset($data[$preferences_field[0]])) {
            $data[$preferences_field[0] . '.' . $preferences_field[1]] = $data[$preferences_field[0]][$preferences_field[1]];
          }

          if (!empty($data[$path])) {
            $field_name  = $value['field'];
            $column = $value['column'];

            // Capture the uuid field.
            if ($path == 'id') {
              $uuid_field = $field_name;
              if (empty($account->{$uuid_field}[LANGUAGE_NONE]['0'][$column])) {
                $link_identity = TRUE;
              }
            }

            $fields[$field_name] = fullcube_webhook_connector_format_data_mapping($field_name, $column, $data, $path);
          }
        }
      }

      // Get and map billing indo.
      $billing_info_mapping = _fullcube_field_mapping_retrieve('billingInfo');
      // Map the billing fields.
      if (!empty($data['billingInfo']) && !empty($billing_info_mapping)) {
        $cc_number = $data['billingInfo']['ccFirstSix'] . 'XXXXXX' . $data['billingInfo']['ccLastFour'];
        $data['billingInfo']['cc_number'] = fullcube_webhook_connector_cc_masking($cc_number);
        foreach ($billing_info_mapping as $billing_data) {
          $billing_field = str_replace('billingInfo.', '', $billing_data['fcpath']);
          $fields[$billing_data['field']][LANGUAGE_NONE]['0'][$billing_data['column']] = $data['billingInfo'][$billing_field];
        }
      }

      // Set the variabel as update from webhook.
      $account->webhook_udpate = TRUE;

      // Make sure we retaing the users eleveated roles
      // and skip the roles configured for non mapping.
      $excluded_roles = variable_get('fullcube_webhook_membership_mapping_exclude', array('administrator'));
      foreach ($excluded_roles as $user_rid => $role) {
        if (!empty($account->roles[$user_rid])) {
          $roles[$user_rid] = $account->roles[$user_rid];
        }
      }
      $account->roles = $roles;
      $account->status = 1;
      user_save($account, $fields);
    }

    // LINK IDENTITY TO FC, check if it exists and if not then link it.
    if ($link_identity) {
      $identity_params = array('uid' => $account->uid, 'id' => $data['id']);
      $link_identity = fullcube_api_link_identity($identity_params);
      if (!$link_identity) {
        watchdog('fullcube_api', 'Error linking personal Identity to FC', array(), WATCHDOG_DEBUG);
      }
    }
  }
  else {
    // Create new user with the data provided.
    $password = user_password(8);

    // Set up the user fields.
    $fields = array(
      'name' => $email,
      'mail' => $email,
      'pass' => $password,
      'status' => 1,
      'init' => 'email address',
      'roles' => $roles,
    );

    // Map the fields specified for mapping.
    if (module_exists('fullcube_field_mapping')) {
      // Get the field mapping.
      $field_mapping = _fullcube_field_mapping_retrieve('profile');
      if (!empty($field_mapping)) {
        foreach ($field_mapping as $value) {
          $query = db_select('fullcube_field_mapping_field', 'wm')
            ->fields('wm', array('path'))
            ->condition('wm.fid', $value['fid'])
            ->range(0, 1)
            ->execute();
          $path_result = $query->fetchObject();
          $path = $path_result->path;

          // Check if the path contains any .
          $preferences_field = explode('.', $path);
          if (count($preferences_field) > 1 && isset($data[$preferences_field[0]])) {
            $data[$preferences_field[0] . '.' . $preferences_field[1]] = $data[$preferences_field[0]][$preferences_field[1]];
          }

          if (!empty($data[$path])) {
            $field_name  = $value['field'];
            $column = $value['column'];
            $fields[$field_name] = fullcube_webhook_connector_format_data_mapping($field_name, $column, $data, $path);
          }
        }
      }
    }

    // Get and map billing indo.
    $billing_info_mapping = _fullcube_field_mapping_retrieve('billingInfo');
    // Map the billing fields.
    if (!empty($data['billingInfo']) && !empty($billing_info_mapping)) {
      $cc_number = $data['billingInfo']['ccFirstSix'] . 'XXXXXX' . $data['billingInfo']['ccLastFour'];
      $data['billingInfo']['cc_number'] = fullcube_webhook_connector_cc_masking($cc_number);
      foreach ($billing_info_mapping as $billing_data) {
        $billing_field = str_replace('billingInfo.', '', $billing_data['fcpath']);
        $fields[$billing_data['field']][LANGUAGE_NONE]['0'][$billing_data['column']] = $data['billingInfo'][$billing_field];
      }
    }

    // The first parameter is left blank so a new user is created.
    $account = user_save('', $fields);

    // If user creation failed, send a error response.
    if (!$account->uid) {
      return FALSE;
    }

    // Manually set the password so it appears in the e-mail.
    $account->password = $fields['pass'];

    // Send the e-mail through the user module.
    _user_mail_notify('register_no_approval_required', $account);

    // Add the user points if enabled.
    if (module_exists('userpoints')) {
      // Get the points configured for membership.
      if ($user_points > 0) {
        $userpoints_params = array(
          'points' => $user_points,
          'uid' => $account->uid,
          'tid' => variable_get('fullcube_webhook_membership_userpoints_tid', userpoints_get_default_tid()),
          'display' => FALSE,
          'description' => 'Userpoints on enroll.',
        );
        $userpoints_result = userpoints_userpointsapi($userpoints_params);
        if (!$userpoints_result) {
          watchdog('fullcube_api', 'Error adding userpoints to user %account_name', array('%acount_name' => $account->name), WATCHDOG_DEBUG);
        }
      }
    }

    // LINK IDENTITY TO FC.
    $identity_params = array('uid' => $account->uid, 'id' => $data['id']);
    $link_identity = fullcube_api_link_identity($identity_params);
    if (!$link_identity) {
      watchdog('fullcube_api', 'Error linking personal Identity to FC', array(), WATCHDOG_DEBUG);
    }
  }

  return TRUE;
}

/**
 * Format the data mapped.
 */
function fullcube_webhook_connector_format_data_mapping($field_name, $column, $data, $path) {
  // Process the data as per field type by getting filed type.
  $field_type = field_info_field($field_name);
  // Multiple the mapping for address field.
  if (isset($field_type['type']) && strpos($field_type['type'], 'addressfield') === 0) {
    $address_field_values = array(
      'thoroughfare' => 'street',
      'locality' => 'city',
      'administrative_area' => 'state',
      'postal_code' => 'zip',
      'country' => 'country',
    );
    if (!isset($data['country'])) {
      $data['country'] = 'US';
    }
    $address_data = array();
    foreach ($address_field_values as $address_key => $address_value) {
      $address_data[$address_key] = (string) $data[$address_value];
    }
    $field_data = array(
      LANGUAGE_NONE => array(
        '0' => $address_data,
      ),
    );
  }
  elseif (isset($field_type['type']) && strpos($field_type['type'], 'date') === 0) {
    // Process date field.
    $date_value = date('Y-m-d\T00:00:00', strtotime($data[$path]));
    $field_data = array(
      LANGUAGE_NONE => array(
        '0' => array(
          $column => $date_value,
        ),
      ),
    );
  }
  elseif (isset($field_type['type']) && strpos($field_type['type'], 'list_text') === 0) {
    $value = $data[$path];
    // Get the option matching the value from allowed list.
    $allowed_values = $field_type['settings']['allowed_values'];
    $value = array_search(drupal_strtolower($value), array_map('strtolower', $allowed_values));

    $field_data = array(
      LANGUAGE_NONE => array(
        '0' => array(
          $column => $value,
        ),
      ),
    );
  }
  else {
    $field_data = array(
      LANGUAGE_NONE => array(
        '0' => array(
          $column => $data[$path],
        ),
      ),
    );
  }
  return $field_data;
}

/**
 * Mask Credit card number.
 */
function fullcube_webhook_connector_cc_masking($cc_number) {
  return drupal_substr($cc_number, 0, 4) . '-' . drupal_substr($cc_number, 4, 4) . '-' . drupal_substr($cc_number, 8, 4) . '-' . drupal_substr($cc_number, 12, 4);
}
