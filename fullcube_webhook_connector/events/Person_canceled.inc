<?php

/**
 * @file
 * Code for the fullcube webhook Person.canceled event processing.
 */

/**
 * Callback for processing Person.canceled webhook.
 */
function fullcube_webhook_connector_process_person_canceled($payload) {
  module_load_include('inc', 'fullcube_webhook_connector', '/events/Person_created');

  $uuid_field = _fullcube_field_retrieve_uuid_field();
  // Check if the user exists in Drupal and have a UUID linked.
  $uuid = $payload['data']['person']['id'];
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
    ->fieldCondition($uuid_field, 'value', $uuid);
  $result = $query->execute();
  $uid = FALSE;
  if (!empty($result['user'])) {
    $users_id = array_keys($result['user']);
    $uid = $users_id[0];
  }
  fullcube_webhook_connector_process_person_created($payload, $uid);
  return TRUE;
}
